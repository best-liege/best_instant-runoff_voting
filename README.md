# BEST instant-runoff voting

Implementation of the new proposed voting system for you to try out.

## Prerequisites

You need to use python 3.
We are also using Pyplot from Matplotlib.

There is a ton of ways to install Python and Python modules, so we leave that up to your internet skills.

## Description

The purpose of this program is to let you experiment with the proposed voting system. Depending on the arguments you use, it will simulate elections and show you the results in two manners:
* graphically with a pie chart regrouping global information
* textually in *output.txt* where you can find all the details on the settings of every sigle simulated election

The textual output is very informative and helps to understand the voting system, but it quickly becomes hard to comprehend for big elections.

## Usage

```
election.py -p <nbProposals> -v <nbVoters> -m <majority> -e <nbElections> --prob-yes=<probabilityOfYes> --prob-no=<probabilityOfNo> --prob-abst=<probabilityOfAbstention> [-a] | -h
```

All arguments are mandatory, except `-h` which displays the above usage information and `-a` which is a flag to set when we want to count abstentions in the vote casts. This is typically the case when a majority of 2/3 is needed.

The value expected by `-m` is a decimal number, not a fraction.

Beware, the code is not very robust beause adding many checks and automatic corrections would make it more difficult to read. Therefore, if you try to be that annoying kid and give negative numbers or arguments of the wrong type you will most likely get a Python error.

Little tip: the values given to `--prob-yes`, `--prob-no`, and `--prob-abst` are normalized. It means that you do not have to give probabilities, only the ratio between the given value matters.

## Example

On Windows 10 with Powershell:
```
& C:/Users/simon/anaconda3/python.exe d:/simon/OneDrive/Documents/BEST/IT/vote_count/election.py -p 3 -v 83 -m 0.5 -e 10000 --prob-yes=38 --prob-no=39 --prob-abst=6
```

On Ubuntu:
```
python3 election.py -p 4 -v 100 -m 0.666 -e 1000 -a --prob-yes=0.7 --prob-no=0.2 --prob-abst=0.1 
```

## Notes on the interpretation of the results

If you want to be sure to understand well the results, we can only advise you to read the code. We will just give a few word of warning here.

Since the order of preference of the "yes" votes is randomized, the simulations systematically correspond to a situation where no (counter-)proposal is favourite and starts with a significant advantage. This lead to a higher ratio of equalities than what we would expect in reality.

Equalities on the pie chart refer to a situation where the last round removed at least two proposals. If the election had a high ratio of "yes" votes, then an equality is likely to mean that all options were popular but since none managed to get a majority they were all discarded. But if the election had instead a high ratio of "no" votes, then it is much less problematic because the discarded choices were probably not popular anyway.

## Author

* **Simon Petit** - *LBG Liège*

## Acknowledgments

* Random strangers on Stack Overflow
