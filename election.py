#!/usr/bin/python

# Implementation of the proposed voting system. Please do not judge my Python too hard.
# Author: Simon PETIT
# Last modified: 06/03/2021

import sys
import getopt
import math
import random
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from bestnames import BestNames

class Election:
   def __init__(self, majority, proposals, voters):
      self.MAJORITY = majority
      self.voters = voters
      self.proposals = proposals      

   # Return
   # - the winner proposal
   # - whether an initial leader won or not
   # - whether the last round was an equality or not
   def run(self, f):
      f.write("--- Simulated voting rounds ---\n")
      winner = None
      initialLeaders = []
      equality = False

      # Used to count the rounds
      i = 0
      # We continue to simulate rounds until we reach a decision
      while True:
         votesCast = 0
         # Initiate vote count at 0 for every proposal
         voteCount = {p: 0 for p in self.proposals}

         for voter in self.voters:
            choice, isCounted = voter.get_vote()
            
            if not isCounted : continue
            else : votesCast += 1
            # Count the vote if possible
            if choice is not None : voteCount[choice] += 1

         # The treshold above which a proposal is approuved. Note that a majority of 1/2 means 50%
         # of the votes + 1.
         threshold = math.floor(self.MAJORITY * votesCast)

         # Log info on current voting round
         f.write("Threshold to exceed: " + str(threshold) + " out of " + str(votesCast) +
                 " votes cast - " + "counts: " + str(voteCount) + "\n")

         maxCount = -1
         for proposal, count in voteCount.items():
            if i == 0:
               # Save intial leader(s)
               if count > maxCount:
                  maxCount = count
                  initialLeaders = [proposal]
               elif count == maxCount:
                  initialLeaders.append(proposal)

            if count > threshold:
               winner = proposal
               break
         
         if winner is not None:
            break

         fewestVotes = min(voteCount.values())
         proposalsToBeRemoved = [p for p, c in voteCount.items() if c == fewestVotes]
         equality = len(proposalsToBeRemoved) > 1

         # Removes all the proposals with the fewest votes
         self.proposals = [p for p in self.proposals if p not in proposalsToBeRemoved]
         if not self.proposals:
            # If there are no more proposals, not proposal is accepted
            break

         # Removes all the proposals with the fewest votes from the choices of the voters
         for v in self.voters:
            v.remove_choices(proposalsToBeRemoved)
         
         i += 1
      
      return winner, winner in initialLeaders, equality

class Voter:
   def __init__(self, name, yess, nos, abstentions, areAbstentionCounted):
      self.NAME = name
      self.AREABSTENTIONCOUNTED = areAbstentionCounted

      self.yess = yess
      self.nos = nos
      self.abstentions = abstentions

   def remove_choices(self, choicesToBeRemoved):
      self.yess = [i for i in self.yess if i not in choicesToBeRemoved]
      self.nos = [i for i in self.nos if i not in choicesToBeRemoved]
      self.abstentions = [i for i in self.abstentions if i not in choicesToBeRemoved]

   # Returns a tuple (choice, isCounted) representing the vote of this voter for a round.
   def get_vote(self):
      # Check if the list is empty
      if self.yess:
         # There is at least one proposal we agree with. We return the first choice.
         return self.yess[0], True
      
      if self.nos:
         # There are no proposal we agree with.
         return None, True
      
      # There are no proposal we agree or disagree with explicitely. Therefore we abstain.
      return None, self.AREABSTENTIONCOUNTED

def main(argv):
   # Parse command line arguments
   try:
      options, _ = getopt.getopt(argv, "p:v:m:e:a", ["prob-yes=", "prob-no=", "prob-abst="])
   except getopt.GetoptError:
      print("election.py -p <nbProposals> -v <nbVoters> -m <majority> -e <nbElections> --prob-yes=<probabilityOfYes> --prob-no=<probabilityOfNo> --prob-abst=<probabilityOfAbstention> [-a] | -h")
      sys.exit(2)

   if len(options) < 7 or (len(options) == 6 and "-a" in [o for o, _ in options]):
      print("These arguments are mandatory: -p <nbProposals> -v <nbVoters> -m <majority> -e <nbElections> --prob-yes=<probabilityOfYes> --prob-no=<probabilityOfNo> --prob-abst=<probabilityOfAbstention>")
      sys.exit(2)

   AREABSTENTIONCOUNTED = False
   for opt, arg in options:
      if opt == '-h':
         print("election.py -p <nbProposals> -v <nbVoters> -m <majority> -e <nbElections> [-a] | -h")
         sys.exit()
      elif opt == "-p":
         nbProposals = int(arg)
      elif opt == "-v":
         nbVoters = int(arg)
      elif opt == "-m":
         majority = float(arg)
      elif opt == "-e":
         nbElections = int(arg)
      elif opt == "--prob-yes":
         probYes = float(arg)
      elif opt == "--prob-no":
         probNo = float(arg)
      elif opt == "--prob-abst":
         probAbst = float(arg)
      elif opt == "-a":
         AREABSTENTIONCOUNTED = True
   
   # Normalize the probabilities of every classification and define thresholds
   sumProb = probYes + probNo + probAbst
   if math.isclose(sumProb, 0.0):
      print("Please give non-zero probabilities")
      sys.exit(2)

   thresholdYes = probYes / sumProb
   thresholdNo = thresholdYes + probNo / sumProb

   # Initialize the pseudo-random number generator with the current time
   random.seed()

   # Initialize the proposals
   proposals = []
   for i in range(nbProposals):
      proposals.append("P" + str(i + 1))

   bestNames = BestNames()
   f = open("output.txt", "w")

   nbRefused = 0
   nbEqualities = 0
   nbWinnerWasLeader = 0
   # Simulates nbElections random elections by changing the votes of the voters
   for e in range(nbElections):
      # Initialize the voters
      voters = []
      for i in range(nbVoters):
         yess = []
         nos = []
         abstentions = []
         for p in proposals:
            decider = random.random()
            if decider <= thresholdYes:
               yess.append(p)
            elif decider <= thresholdNo:
               nos.append(p)
            else:
               abstentions.append(p)

         # The preference order is randomized because we don't know better, but this will create more
         # equalities than in a real situation where some candidates are favourites
         random.shuffle(yess)

         voters.append(Voter(bestNames.get(i), yess, nos, abstentions, AREABSTENTIONCOUNTED))

      # Log info on the current setting
      f.write("Election " + str(e + 1) + "\n")
      if AREABSTENTIONCOUNTED:
         f.write("Abstentions are counted in the vote casts\n")
      else:
         f.write("Abstentions are not counted in the vote casts\n")

      f.write("--- Votes ---\n")
      for v in voters:
         f.write(v.NAME + "\n\tYes: " + str(v.yess) + "\n\tNo: " +
                 str(v.nos) + "\n\tAbstention: " + str(v.abstentions) + "\n")

      # Initialize and run election
      election = Election(majority, proposals, voters)
      winner, initialLeaderWon, equality = election.run(f)

      # Log results
      f.write("--- Result ---" + "\n")
      if winner is not None:
         f.write("Proposal " + winner + " is accepted\n")
         if initialLeaderWon:
            nbWinnerWasLeader += 1
            f.write("The winner was initially leading\n")
         else:
            f.write("The winner was not initially leading\n")
      else:
         nbRefused += 1
         f.write("No proposal is accepted\n")
         if equality:
            nbEqualities += 1
            f.write("The last round was an equality\n")
      f.write("===============================================================================\n")
   
   f.close()

   # Plot results
   nbAccepted = nbElections - nbRefused
   nbWinnerWasNotLeader = nbAccepted - nbWinnerWasLeader
   nbNoEqualities = nbRefused - nbEqualities

   _, axs = plt.subplots(3, 1)

   labelsGeneral = ["A proposal was accepted", "All proposals were refused"]
   labelsAccepted = ["Winner was leader", "Winner was not leader"]
   labelsRefused = ["Equality", "No equality"]

   axs[0].pie([nbAccepted, nbRefused], labels=labelsGeneral, labeldistance=1.25,
                autopct="%1.1f%%", colors=["green", "crimson"])
   axs[0].set(aspect="equal", title="Summary of " + str(nbElections) + " elections")

   if nbAccepted > 0:
      axs[1].pie([nbWinnerWasLeader, nbWinnerWasNotLeader], labels=labelsAccepted,
                 labeldistance=1.25, autopct="%1.1f%%", colors=["limegreen", "forestgreen"])
      axs[1].set(aspect="equal", title="Accepted proposals details")
   else:
      axs[1].set_visible(False)

   if nbRefused > 0:
      axs[2].pie([nbEqualities, nbNoEqualities], labels=labelsRefused, labeldistance=1.25,
                autopct="%1.1f%%", colors=["orangered", "red"])
      axs[2].set(aspect="equal", title="Refused proposals details")
   else:
      axs[2].set_visible(False)

   # Spaces the subplots
   plt.tight_layout(h_pad=2)
   plt.show()


if __name__ == "__main__":
   main(sys.argv[1:])
